package domparser;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreateXMLDoc {

    public static void main(String[] args) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            // db;

            DocumentBuilder db;
            try {
                db = dbf.newDocumentBuilder();

                Document doc = db.newDocument();

                Element element = doc.createElement("profile");
                doc.appendChild(element);
                Attr attr = doc.createAttribute("id");
                attr.setNodeValue("5830407557");
                element.setAttributeNode(attr);

                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode("Kannikar Prompa"));
                element.appendChild(name);

                Element major = doc.createElement("major");
                major.appendChild(doc.createTextNode("Computer Engineering"));
                element.appendChild(major);

                Element area = doc.createElement("area");
                area.appendChild(doc.createTextNode("Software"));
                major.appendChild(area);//

                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer tran = tf.newTransformer();
                DOMSource source = new DOMSource(doc);

                StreamResult sr = new StreamResult(new File("src\\domparser\\profile2.xml"));
                tran.transform(source, sr);

            } catch (ParserConfigurationException ex) {
                Logger.getLogger(CreateXMLDoc.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Successful");
        } catch (TransformerException ex) {
            Logger.getLogger(CreateXMLDoc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
