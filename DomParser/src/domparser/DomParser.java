/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domparser;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.DoubleStream.builder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author IzaBellasan
 */
public class DomParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            try {
                Document doc = builder.parse("src\\domparser\\nation.xml");
                NodeList personList = doc.getElementsByTagName("nation");
                Node rootNode = personList.item(0);
                String rootElementName = rootNode.getNodeName();
                System.out.println("The root element name is " + rootElementName);
                
            
                Element rootElement = (Element)rootNode;
                String rootAttr = rootElement.getAttribute("id");
                System.out.println("The root element attribute is id=" + rootAttr);
                
            } catch (SAXException ex) {
                Logger.getLogger(DomParser.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DomParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DomParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
