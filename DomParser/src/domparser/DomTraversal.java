/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domparser;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomTraversal {

    public static void main(String[] args) throws ParserConfigurationException, IOException {

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("src\\domparser\\nation.xml");
            Node node = doc.getDocumentElement();
            /*System.out.println(node.getNodeName());
            if(node.hasChildNodes()){
            System.out.println(node.getChildNodes().getLength());
            System.out.println(node.getChildNodes().item(1));
            }*/
            writeNode(node);

        } catch (SAXException ex) {
            Logger.getLogger(DomTraversal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeNode(Node node) {
        short nodeTypeShort;
        String nodeTypeString = "";
        String nodeName = "";
        String nodeValue = "";
        Node currentNode = null;
        if (node.hasChildNodes()) {
            for (int n = 0; n < node.getChildNodes().getLength(); n++) {

                currentNode = node.getChildNodes().item(n);
                nodeTypeShort = currentNode.getNodeType();
                if (nodeTypeShort == 1) {
                    nodeTypeString = "element";
                } else if (nodeTypeShort == 3) {
                    nodeTypeString = "text";
                }
                System.out.print("Node: type is " + nodeTypeString);
                nodeName = currentNode.getNodeName();
                System.out.print(" name is " + nodeName);
                nodeValue = (String) currentNode.getNodeValue();
                System.out.println(" value is " + nodeValue);
                writeNode(currentNode);
            }
        }

    }
}
